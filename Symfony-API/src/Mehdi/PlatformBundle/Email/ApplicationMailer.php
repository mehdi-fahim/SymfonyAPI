<?php
// src/Mehdi/PlatformBundle/Email/ApplicationMailer.php

namespace Mehdi\PlatformBundle\Email;

use Mehdi\PlatformBundle\Entity\Application;

class ApplicationMailer
{
  /**
   * @var \Swift_Mailer
   */
  private $mailer;

  public function __construct(\Swift_Mailer $mailer){

    $this->mailer = $mailer;
  }

  public function sendNewNotification(Application $application){
    
    $message = new \Swift_Message(
      'Nouvelle candidature',
      'Vous avez reçu une nouvelle candidature.');

    $message
      ->addTo($application->getAdvert()->getAuthor()) // Ici bien sûr il faudrait un attribut "email", j'utilise "author" à la place
      ->addFrom('diims93tkt@gmail.com');

    $this->mailer->send($message);
  }
}
