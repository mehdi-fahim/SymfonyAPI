## Pré-requis pour lancer le projet

###  Installer composer avec la commande (MAC)

1 `php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"`

2 `php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"`

3 `php composer-setup.php`

4 `php -r "unlink('composer-setup.php');"`

### Installer composer avec la commande (Windows):

Télécharger le .exe sur : 

https://getcomposer.org/doc/00-intro.md#downloading-the-composer-executable

### Après avoir cloner le projet

- Aller à la racine du projet

- Lancer la commande `php composer.phar update` pour mettre à jour les dépendances.

- Un fichier `parameters.yml` à été généré dans le dossier `app/config`

```yaml
parameters:
    database_host: lhôte de votre site
    database_port: le port si il y en a un
    database_name: le nom de votre BDD
    database_user: votre nom dutilisateur
    database_password: votre mot de passe
``` 

### Base de données

1 - lancer la commande `php bin/console doctrine:database:create` pour créer la BDD

2 - Puis la commande `php bin/console doctrine:generate:entity` pour créer l'entité et le repertoire

3 - Ensuite entré la commande `php bin/console doctrine:schema:update --dump-sql` pour créer la table avec le champs rentré dans la création de l'entité

4 - Enfin la commande `php bin/console doctrine:schema:update --force` pour envoyer la requète à la BDD